@extends('layouts.app')

@section('title', 'Candidate Details')

@section('content')       
       <h1>Candidate Details</h1>
        <form method = "post" action = "{{action('CandidatesController@candidateDetails',$candidate->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label>Candidate Name : {{$candidate->name}}</label>
        </div>     
        <div class="form-group">
            <label>Candiadte Email : {{$candidate->email}}</label>
        </div>
        <div class="form-group">
            <label>Candidate Age : {{$candidate->age}}</label>
        </div>
        <div class="form-group">
        <label>
            @if(isset($candidate->user_id))
                Candidate Owner : {{$candidate->owner->name}}  
            @else
            Candidate owner : No Owner Yet
            @endif
        </label>
        </div>
        <div class="form-group">
            <label>Candidate status : </label>
        </div> 
        <div class="dropdown">
                    @if (null != App\Status::next($candidate->status_id))    
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($candidate->status_id))
                           {{$candidate->situation->name}}
                        @else
                            Define status
                        @endif
                    </button>
                    @else 
                    {{$candidate->situation->name}}
                    @endif
                                                   
                    @if (App\Status::next($candidate->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($candidate->status_id) as $status)
                         <a class="dropdown-item" href="{{route('candidate.showstatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
                </div>
            <br>
                <a  href="{{route('candidate.changestatus',[$candidate->id,$candidate->status_id])}}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Update Candidate</a>                         
        </form>    
    </body>
</html>
@endsection