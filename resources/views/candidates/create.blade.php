@extends('layouts.app')

@section('title','Create Candidate')

@section('content')
    <h1>Create Candidate</h1>
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method = "post" action = "{{action('CandidatesController@store')}}"> <!-- transfer the data to store -->
        @csrf <!-- protect from csrf attack -->
        <div class="form-group">
            <label for = "name">Candidate name</label>
            <input type = "text" name = "name" class="form-control" placeholder = "Enter Name">
        </div>
        <div class="form-group">
            <label for = "email">Candidate email</label>
            <input type = "text" name = "email" class="form-control" placeholder = "Enter Email">
        </div>
        <div class="form-group">
            <label for = "email">Candidate age</label>
            <input type = "text" name = "age" class="form-control" placeholder = "Enter Age">
        </div>
        <div>
            <input class="btn btn-outline-primary" type = "submit" name = "submit" value = "Create candidate">
        </div>
    </form>
@endsection
