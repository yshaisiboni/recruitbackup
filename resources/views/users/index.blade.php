@extends('layouts.app')

@section('title','User list')

@section('content')
        <h1>Users List</h1>
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
        @endif
        <table class="table table-striped">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
            </tr>
            <!-- table data -->
            @foreach($users as $user)
                <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->department_id}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
                </td> 
                <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
                </td>
                </tr>
            @endforeach
    </table>
@endsection