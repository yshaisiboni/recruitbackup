<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Candidate extends Model
{

    use Sortable;

    protected $fillable = ['name','email','age']; // which fields I allow to update

    public $sortable = ['age'];

    public function owner(){ // user is the owner of candidate
        return $this->belongsTo('App\User', 'user_id'); // each candidate has only one user (owner)
    }

    public function situation(){ 
        return $this->belongsTo('App\Status', 'status_id'); // each candidate has only one status
    }
}
