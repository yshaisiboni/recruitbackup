<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

// full name is : "App\Http\Controllers"
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::sortable()->paginate(5);
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates', 'users', 'statuses'));
    }

    public function changeUser($cid,$uid = null){
        Gate::authorize('assign-user');
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        // return redirect('candidates');
        return back();
    }

    public function changeStatus($cid,$sid){
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status', $candidate)){
            $stageFrom = $candidate->status_id;
            $chk = Status::allowed($sid,$stageFrom);
            if($chk == TRUE){
                $candidate->status_id = $sid;
                $candidate->save();
            }
        }
        else {
            Session::flash('notallowed', 'You are not allowed to change the status of the candidate because you are not the owner of the candidate');
        }
        //return redirect('candidates');
        return back();
    }

    public function showStatus($cid,$sid = null){
        $candidate = Candidate::findOrFail($cid);
        $candidate->status_id = $sid;
        return view('candidates.details', compact('candidate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'age' => 'required'
        ]);
        $candidate = new Candidate();
        // $candidate->name = $request->name;
        // $candidate->email = $request->email;
        $candidate->create($request->all());
        return redirect('candidates'); // transfer the user to candidates
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id); // check if object exists
        return view('candidates.edit', compact('candidate', 'id')); //Transfer all candidate table values ; name, email, id etc
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'age' => 'required'
        ]);
        $candidate = Candidate::findOrFail($id);
        $candidate->update($request->all());// update all data
        return redirect()->route('candidates.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');
    }

    public function myCandidates()
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $candidates = $user->candidates;
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates', 'users', 'statuses'));
    }

    public function candidateDetails($id)
    {
        $candidate = Candidate::findOrFail($id);       
        return view('candidates.details', compact('candidate'));
    }

}
